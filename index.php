 <?php  
 	require('animal.php');
 	require('ape.php');
 	require('frog.php');

 	$animal = new animal("Kangguru");
 	echo "Nama Hewan = " . $animal->name . "<br>";
 	echo "Jumlah Kaki = " .$animal->kaki . "<br>";
 	echo "Fungsi Kaki = ";
 	echo var_dump($animal->jump) ;
 	echo "<br><br>";

 	$ape = new ape("Monyet");
 	echo "Nama Hewan = " . $ape->name . "<br>";
 	echo "Jumlah Kaki = " . $ape->kaki . "<br>";
 	echo "Fungsi Kaki = ";
 	echo var_dump($ape->jump);
 	echo "<br><br>";

 	$frog = new frog("Katak");
 	echo "Nama Hewan = " . $frog->name . "<br>";
 	echo "Jumlah Kaki = " . $frog->kaki . "<br>";
 	echo "Fungsi Kaki = ";
 	echo var_dump($frog->jump);
?>